let http = require('http');
let fs = require('fs').promises;
let url = require('url');
let events = require('events');
let port = 8080;

http.createServer(async (req, res) => {

    console.log(`${new Date().toUTCString()} - ${req.url}`); 

    try {
        let data = await fs.readFile('.' + req.url);
        res.write(data);
        res.end();   
    } catch (error) {
        console.log(error);
        res.end('Ooops!');   
    }

    // fs.readFile('.' + req.url, function(err, data) {
    //     if (err) {
    //         console.log(err);
    //         res.end('Ooops!');
    //     }
    //     res.write(data);
    //     res.end();
    // });
    
}).listen(port);

console.log(`Listening on port ${port}`);





















// let http = require('http');
// let fs = require('fs');
// let port = 8080;

// http.createServer((req, res) => {

//     console.log(`${new Date().toUTCString()} - ${req.url}`); 

//     fs.readFile('.' + req.url, function(err, data) {
//         if (err) {
//             console.log(err);
//             res.end('Ooops!');
//         }

//         res.write(data);
//         res.end();
//     });

//     let log = `${new Date().toUTCString()} - ${req.url}\n`;
//     fs.appendFile('log.txt', log, function(err) {
//         if (err) {
//             console.log('LOG ERROR!!!');
//         } else {
//             console.log('DONE!!!'); 
//         }
//     });

// }).listen(port);

// console.log(`Listening on port ${port}`);

















// let http = require('http');
// let port = 8080;

// http.createServer(function(req, res) {

//     console.log(`${new Date().toUTCString()} - ${req.url}`); 

//     res.write('My name is Gleb!\n');
//     res.write('Hello, world!');

//     res.end();

// }).listen(port);

// console.log(`Listening on port ${port}`);











// let mydate = require('./mydate');
// console.log('Hello, world!');
// console.log(mydate.getDate());
// mydate.test();
// console.log(mydate.pi);
